package com.atelier.catmash.controllers;

import com.atelier.catmash.entities.Cat;
import com.atelier.catmash.services.CatTableAcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cats")
public class CatController {

    @Autowired
    private CatTableAcess catTableAcess;

    @GetMapping
    public List<Cat> getAllCats(){
        return catTableAcess.findAllCats();
    }
}

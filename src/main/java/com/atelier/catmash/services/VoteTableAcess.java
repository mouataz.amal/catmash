package com.atelier.catmash.services;

import com.atelier.catmash.entities.Vote;
import com.atelier.catmash.exceptions.VoteNotFoundException;
import com.atelier.catmash.repositories.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class VoteTableAcess {

    @Autowired
    VoteRepository voteRepository;

    public Vote findVoteById( Long id){
        return voteRepository.findById(id).orElseThrow(() -> new VoteNotFoundException(id));
    }



}

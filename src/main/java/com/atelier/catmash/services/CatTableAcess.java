package com.atelier.catmash.services;

import com.atelier.catmash.entities.Cat;
import com.atelier.catmash.exceptions.CatNotFoundException;
import com.atelier.catmash.repositories.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CatTableAcess {

    @Autowired
    private CatRepository catRepository;

    public Cat findCatById(Long id){
        return catRepository.findById(id).orElseThrow(() -> new CatNotFoundException(id));
    }

    public List<Cat> findAllCats(){
        return catRepository.findAll();
    }
}

package com.atelier.catmash.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class VoteNotFoundException extends RuntimeException {
    public VoteNotFoundException(Long id) {
        super("Vote with Id : " + id + " not found");
    }
}

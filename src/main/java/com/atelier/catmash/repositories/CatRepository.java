package com.atelier.catmash.repositories;

import com.atelier.catmash.entities.Cat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CatRepository extends JpaRepository <Cat,Long> {

    Optional<Cat> findByName(String name);
}

package com.atelier.catmash.repositories;

import com.atelier.catmash.entities.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoteRepository extends JpaRepository<Vote, Long> {
}
